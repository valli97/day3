package com.booking.controllerTest;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.booking.controller.BusDetailsController;
import com.booking.dto.BusDeatilsDto;
import com.booking.dto.ResponseBusDeatilsDto;
import com.booking.service.BusDetailsService;


@RunWith(MockitoJUnitRunner.class)
public class BusDetailsControllerTest {
	@InjectMocks
	BusDetailsController busDetailsController;

	@Mock
	BusDetailsService busDetailsService;

	@Test
	public void TestSearchByBusDetailsForPositive() {

		ResponseBusDeatilsDto busdetails = new ResponseBusDeatilsDto();
		busdetails.setMessage("success");

		BusDeatilsDto busdto = new BusDeatilsDto();
		busdto.setDay("Monday");
		busdto.setDestination("madhurai");
		busdto.setSource("enjoyment");

		Mockito.when(busDetailsService.searchDetails(busdto)).thenReturn(Mockito.any());

		ResponseEntity<ResponseBusDeatilsDto> responsebusdetailsdto = busDetailsController.searchByBusDetails(busdto);
		assertEquals(HttpStatus.OK, responsebusdetailsdto.getStatusCode());

	}

	@Test
	public void TestAddByBusDetailsForPositive() {

		ResponseBusDeatilsDto busdetails = new ResponseBusDeatilsDto();
		busdetails.setMessage("success");

		BusDeatilsDto busdto1 = new BusDeatilsDto();
		busdto1.setDay("Monday");
		busdto1.setDestination("madhurai");
		busdto1.setSource("enjoyment");

		Mockito.when(busDetailsService.saveBusDetails(Mockito.any())).thenReturn(Mockito.any());
		ResponseEntity<ResponseBusDeatilsDto> responsebusdetailsdto = busDetailsController.AddByBusDetails(busdto1);
		assertEquals(HttpStatus.OK, responsebusdetailsdto.getStatusCode());

	}

}
