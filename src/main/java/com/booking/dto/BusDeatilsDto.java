package com.booking.dto;



import com.fasterxml.jackson.annotation.JsonProperty;

public class BusDeatilsDto {
	@JsonProperty
	private String source;
	@JsonProperty
	private String destination;
	@JsonProperty
	private String day;
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	
	
	
}