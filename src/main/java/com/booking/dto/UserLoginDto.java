package com.booking.dto;



import com.fasterxml.jackson.annotation.JsonProperty;

public class UserLoginDto {
	@JsonProperty
	private String emailId;
	@JsonProperty
    private String password;
	
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
