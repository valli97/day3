package com.booking.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class TicketDto {

	private Integer ticketId;

	private Integer userId;
	
	private Integer busId;

	private String reservationDate;

	private Integer ticketNumber;
	
	private Integer numberofSeat;
	
	

	public Integer getNumberofSeat() {
		return numberofSeat;
	}

	public void setNumberofSeat(Integer numberofSeat) {
		this.numberofSeat = numberofSeat;
	}

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(String reservationDate) {
		this.reservationDate = reservationDate;
	}

	public Integer getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(Integer ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public Integer getBusId() {
		return busId;
	}

	public void setBusId(Integer busId) {
		this.busId = busId;
	}
	
	

}
