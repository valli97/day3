package com.booking.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.booking.dto.TicketBookingDto;
import com.booking.dto.TicketDto;
import com.booking.model.TicketBooking;
import com.booking.model.User;
import com.booking.service.TicketBookingService;

@RestController
public class TicketBookingController {

	@Autowired
	private TicketBookingService bookingService;
	
	TicketBookingDto booking2 = new TicketBookingDto();

	@PostMapping("/book")
	public ResponseEntity<TicketBookingDto> bookingtheTicket(@RequestBody TicketDto ticketDto) {
		booking2 = bookingService.bookTheSeat(ticketDto);
		return new ResponseEntity<TicketBookingDto>(booking2, HttpStatus.CREATED);
	}
	
	/*
	 * @GetMapping("/book1") ResponseEntity<TicketBooking> bookingHistory(Integer
	 * ticketId) { booking2=bookingService.passengerDetails(ticketId); return new
	 * ResponseEntity<TicketBooking>(HttpStatus.OK);
	 * 
	 * 
	 * 
	 * }
	 */
	

}
