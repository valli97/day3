package com.booking.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.booking.dto.UserLoginDto;
import com.booking.exceptions.UserNotFoundException;
import com.booking.model.User;
import com.booking.repository.UserRepository;

@Service
public class UserLoginService {
	@Autowired
	UserRepository userRepository;

	private User userRegistration = new User();

	public Optional<User> checkLoginByUserId(UserLoginDto userlogindto) {
		BeanUtils.copyProperties(userlogindto, userRegistration);
		Optional<User> login = userRepository.findByEmailIdAndPassword(userlogindto.getEmailId(),
				userlogindto.getPassword());
		if (!login.isPresent()) {
			throw new UserNotFoundException("Enter valid emaild and password");
		} else {
			return login;

		}
	}

}
