package com.booking.model;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SeatBooking {
	@Id
	private Long seatid;
	private String type;

	public Long getSeatid() {
		return seatid;
	}

	public void setSeatid(Long seatid) {
		this.seatid = seatid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
