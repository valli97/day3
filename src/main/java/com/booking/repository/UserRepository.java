package com.booking.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.booking.model.User;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
	public Optional<User> findByEmailIdAndPassword(String emailId, String password);
}
